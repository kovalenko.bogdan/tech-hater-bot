#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Simple Bot to hate IT technologies end instruments.

Replies to messages that contains given technologies in the text with stickers

Usage:
Add bot to chat or start private conversation with him
and send message with technology name i.e. 'python'
"""

import logging

from telegram import Bot, ParseMode
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

# Enable logging
logging.basicConfig(filename='bot.log', format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

bot = Bot("802998781:AAFiAdfWjyU_FvyitJCz0iBQjVNG89eQ7R0")
ss = bot.get_sticker_set('Hate_everything')
updater = Updater("802998781:AAFiAdfWjyU_FvyitJCz0iBQjVNG89eQ7R0", use_context=True)

STICKER_MAP = {
    'CAADAgADDAADzM9oDm2Pe20yWZbhAg': ('angular', 'ангуляр'),
    'CAADAgADSQADzM9oDsWgmTf0XdbPAg': ('native', 'нейтив'),
    'CAADAgADDQADzM9oDv5shpX0hIfxAg': ('react', 'реакт'),
    'CAADAgADhwADzM9oDptOly5_1pF8Ag': ('CAADAgADhwADzM9oDptOly5_1pF8Ag',),
    'CAADAgADDgADzM9oDsLvN9RlgmeKAg': ('bootstrap', 'бутстрап'),
    'CAADAgADDwADzM9oDvDDuYXuZrGeAg': ('vue', 'vue.js', 'вьюджс'),
    'CAADAgADEAADzM9oDqrNKHWesQYaAg': ('CAADAgADEAADzM9oDqrNKHWesQYaAg',),
    'CAADAgADaQADzM9oDkFQZNtb2o40Ag': ('ember', 'эмбер', 'ембер'),
    'CAADAgADEQADzM9oDrI7cw9_GybXAg': ('java', 'джава'),
    'CAADAgADEgADzM9oDmrXqGSGLOuRAg': ('php', 'пхр'),
    'CAADAgADEwADzM9oDrYK34NQaVBmAg': ('python', 'питон', 'пайтон'),
    'CAADAgADFAADzM9oDnRq-GZaXsbnAg': ('ruby', 'руби'),
    'CAADAgADFQADzM9oDi8DjDwoDPirAg': ('.net', 'дот нет', 'дотнет'),
    'CAADAgADJgADzM9oDrdcvx9-4j7hAg': ('visualstudio', 'вижуалстудио'),
    'CAADAgADJwADzM9oDtyhZJHQxuWkAg': ('webstorm', 'вебшторм', 'вебсторм'),
    'CAADAgADKAADzM9oDpfC_oEG_RL4Ag': ('atom', 'атом'),
    'CAADAgADKQADzM9oDqM8ELauyHbJAg': ('sublime', 'саблайм'),
    'CAADAgADKgADzM9oDhZXk8r390XbAg': ('CAADAgADKgADzM9oDhZXk8r390XbAg',),
    'CAADAgADKwADzM9oDmlSXVObGrGtAg': ('ubuntu', 'убунту', 'убунта'),
    'CAADAgADLAADzM9oDvijS-GbyKgnAg': ('windows', 'виндовс', 'винда'),
    'CAADAgADLgADzM9oDlj26cX7jF5qAg': ('osx', 'macos', 'макос'),
    'CAADAgADLwADzM9oDnr7NYhorq72Ag': ('android', 'андроид'),
    'CAADAgADMAADzM9oDvaLqlzY_vQOAg': ('ios', 'айос'),
    'CAADAgADMQADzM9oDkWpnaiM3nrnAg': ('bower', 'бовер', 'боувер'),
    'CAADAgADMgADzM9oDjVQWuJHaYgKAg': ('CAADAgADMgADzM9oDjVQWuJHaYgKAg',),
    'CAADAgADMwADzM9oDvoef4Q0diE1Ag': ('facebook', 'фейсбук'),
    'CAADAgADNAADzM9oDjSA_Jw5iH-1Ag': ('vkontakte', 'вконтакте'),
    'CAADAgADNQADzM9oDqXDTbvCD04MAg': ('go', 'гоу'),
    'CAADAgADNgADzM9oDlWU8SCSVmtoAg': ('gulp', 'галп', 'гулп'),
    'CAADAgADNwADzM9oDo10eXktTgUnAg': ('node', 'нода'),
    'CAADAgADOAADzM9oDhcpWg3PqyWBAg': ('npm', 'нпм'),
    'CAADAgADOQADzM9oDlQQUnJ-7PltAg': ('CAADAgADOQADzM9oDlQQUnJ-7PltAg',),
    'CAADAgADOgADzM9oDpMuMnxQ-DUGAg': ('phpstorm', 'пхпшторм', 'пхпсторм'),
    'CAADAgADOwADzM9oDjXy7_ISwocqAg': ('photoshop', 'фотошоп'),
    'CAADAgADPAADzM9oDpnchNA7vn_zAg': ('CAADAgADPAADzM9oDpnchNA7vn_zAg',),
    'CAADAgADPQADzM9oDkNwlZeYNEPEAg': ('scala', 'скала'),
    'CAADAgADPgADzM9oDrYGOss-dJCOAg': ('webpack', 'вебпак', 'вэбпак'),
    'CAADAgADPwADzM9oDvy2irp0Pt5rAg': ('CAADAgADPwADzM9oDvy2irp0Pt5rAg',),
    'CAADAgADQAADzM9oDgKK0B3OVRglAg': ('yarn', 'ярн'),
    'CAADAgADQQADzM9oDsKX_Qf04rUcAg': ('sketch', 'скетч'),
    'CAADAgADQgADzM9oDiqThRRbFEHIAg': ('figma', 'фигма'),
    'CAADAgADQwADzM9oDkyatrnFUAGtAg': ('CAADAgADQwADzM9oDkyatrnFUAGtAg',),
    'CAADAgADRAADzM9oDpwkLblNpbFrAg': ('c++', 'с++', 'плюсы', 'кресты'),
    'CAADAgADRQADzM9oDlugcwAB5xNG-gI': ('CAADAgADRQADzM9oDlugcwAB5xNG-gI',),
    'CAADAgADRgADzM9oDsTkwkTaP6x3Ag': ('kafka', 'кафка'),
    'CAADAgADRwADzM9oDhh_5xbs5hNPAg': ('kotlin', 'котлин'),
    'CAADAgADSAADzM9oDu272wmXZt-fAg': ('rabbit', 'ребит'),
    # 'CAADAgADSQADzM9oDsWgmTf0XdbPAg': ('native', 'нейтив'), moved to the top
    'CAADAgADSgADzM9oDuGN3VAr5m6dAg': ('CAADAgADSgADzM9oDuGN3VAr5m6dAg',),
    'CAADAgADSwADzM9oDrZ4cNuJYuX5Ag': ('debian', 'дебиан'),
    'CAADAgADTAADzM9oDvZPU6m40yAbAg': ('arch', 'арч'),
    'CAADAgADTQADzM9oDkXQ2LoCQARzAg': ('gentoo', 'генту'),
    'CAADAgADUAADzM9oDsDmzWBVRgK8Ag': ('twitter', 'твитер', 'твиттер'),
    'CAADAgADUQADzM9oDvMGfDDBP5XXAg': ('sass', 'scss', 'сасс'),
    'CAADAgADUwADzM9oDknKiHbiQgOcAg': ('stylus', 'стилус'),
    'CAADAgADVAADzM9oDqWXc6WmDoCqAg': ('CAADAgADVAADzM9oDqWXc6WmDoCqAg',),
    'CAADAgADVgADzM9oDoMXDp4DOdvUAg': ('1c', '1с'),
    'CAADAgADWAADzM9oDkZDUz_z4tQ9Ag': ('less', 'лесс', 'лэсс'),
    'CAADAgADWQADzM9oDqnwqvohCLJWAg': ('lebedev', 'артемий', 'лебедев'),
    'CAADAgADWgADzM9oDnYEYXSRxMx0Ag': ('js', 'javascript', 'джс'),
    'CAADAgADWwADzM9oDiG1Oyz6b3LMAg': ('html', 'хтмл'),
    'CAADAgADXAADzM9oDouBdaD8TBgEAg': ('css', 'цсс'),
    'CAADAgADZAADzM9oDmxRAdjd8rCGAg': ('CAADAgADZAADzM9oDmxRAdjd8rCGAg',),
    'CAADAgADhgADzM9oDrrUPrntI-cTAg': ('CAADAgADhgADzM9oDrrUPrntI-cTAg',),
}


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.

def help(update, context):
    """Send the STICKER_MAP obj when the command /help is issued."""
    bot.send_message(chat_id=update.message.chat_id,
                     text='```' + str(STICKER_MAP) + '```',
                     parse_mode=ParseMode.MARKDOWN)


def send_sticker(update, context):
    """Reply with sticker."""
    for sticker_id, match_words in STICKER_MAP.items():
        if update.message:
            if any([word in update.message.text.replace(' ', '').lower() for word in match_words]):
                logger.info('Sticker sent: "%s" Chat "%s"', match_words, update.message.chat.title or
                            update.message.chat.username)
                return update.message.reply_sticker(sticker_id, reply_to_message_id=None)


def list_stickers(update, context):
    """Print all stickers from pack with theirs id's.
    Used it to print stickers and their id's during development"""
    for item in ss['stickers']:
        bot.send_sticker(update.message.chat_id, item['file_id'])
        bot.send_message(update.message.chat_id, item['file_id'])


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    """Start the bot."""
    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    dp.add_handler(CommandHandler("help", help))

    dp.add_handler(MessageHandler(Filters.text, send_sticker))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()